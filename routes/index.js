var express = require('express')
var router = express.Router()
const fs = require('fs')
const path = require('path')
const sendMessage = require('../lib/sendMessage')
const jwt = require('../lib/jwt')
var debug = require('debug')('stride:index-route')

/* GET home page. */
router.get('/', function(req, res) {
  res.redirect('/descriptor')
})

router.get('/descriptor', (req, res) => {
  let descriptor = JSON.parse(
    fs.readFileSync(path.join(__dirname, '../app-descriptor.json')).toString()
  )
  descriptor.baseUrl = 'https://' + req.headers.host
  res.contentType = 'application/json'
  res.send(descriptor)
  res.end()
})

router.post('/installed', jwt.verifyRequest, (req, res) => {
  // context.cloudId        - the site id that contains the conversation that the app was installed in.
  // context.userId         - the user id that installed the app.
  // context.conversationId - the conversation id that the app was installed in.
  let context = {
    cloudId: req.body.cloudId,
    userId: req.body.userId,
    conversationId: req.body.resourceId
  }

  //TODO: save context to a persistent data store for reference later.
  debug('Installed payload context:', context)

  //Send a message into the room to tell everyone about our app that was just installed.
  sendMessage(
    '🎉 The Stride sample app demonstrating how to send a message 💌 has been installed. 🚀',
    context
  )

  res.status(200).end()
})

router.post('/uninstalled', jwt.verifyRequest, (req, res) => {
  let context = {
    cloudId: req.body.cloudId,
    userId: req.body.userId,
    conversationId: req.body.resourceId
  }

  //TODO: Use context to remove the data from your data store
  debug('Uninstalled payload:', context)

  res.status(200).end()
})

module.exports = router
