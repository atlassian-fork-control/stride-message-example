const app = require('../../app')
const request = require('supertest')
const jwt = require('jsonwebtoken')

const mockProcess = {
  env: {
    CLIENT_ID: 'fake123',
    CLIENT_SECRET: 'fake123secret'
  }
}

const token = jwt.sign(
  {
    iss: mockProcess.env.CLIENT_ID,
    sub: '557057:729b34f7-b3a3-4dda-9883-b1b9ed7dc794',
    jti: 'adf866af-d93f-44da-b9b9-d83dd3c47463',
    iat: Date.now(),
    exp: Date.now() + 3600,
    context: {
      cloudId: 'a436116f-02ce-4520-8fbb-7301462a1674',
      resourceType: 'conversation',
      resourceId: 'be69acf9-26c6-4f99-ab55-dc8b1e5aef80'
    },
    hash: '21f571b8cf0f6afce6fcadf7e2e63820e810d089fd95bdb7c8717999612808e0'
  },
  mockProcess.env.CLIENT_SECRET
)

test('make sure descriptor route is setup', () => {
  request(app)
    .get('/descriptor')
    .then(res => {
      expect(res.status).toBe(200)
      expect(res.headers['content-type']).toMatch(/^(application\/json)/)
      expect(res.body.baseUrl).toMatch(/^(https:\/\/)+/)
      expect(typeof res.body).toBe('object')
    })
})

test('make sure installed route is set up', done => {
  return request(app)
    .post('/installed')
    .set('authorization', 'Bearer ' + token)
    .set('mockProcess', JSON.stringify(mockProcess))
    .send({
      key: 'sample-app',
      productType: 'chat',
      cloudId: 'xxx',
      resourceType: 'conversation',
      resourceId: 'yyy',
      eventType: 'installed',
      userId: 'zzz',
      oauthClientId: 'aaa',
      version: '1'
    })
    .then(function(res) {
      expect(res.status).toBe(200)
      done()
    })
})

test('make sure uninstalled route is set up', done => {
  return request(app)
    .post('/uninstalled')
    .set('authorization', 'Bearer ' + token)
    .set('mockProcess', JSON.stringify(mockProcess))
    .send({
      key: 'sample-app',
      productType: 'chat',
      cloudId: 'xxx',
      resourceType: 'conversation',
      resourceId: 'yyy',
      eventType: 'uninstalled',
      userId: 'zzz',
      oauthClientId: 'aaa',
      version: '1'
    })
    .then(function(res) {
      expect(res.status).toBe(200)
      done()
    })
})
