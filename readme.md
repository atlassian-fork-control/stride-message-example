# Stride - Send a message

This is an example app for Atlassian Stride.

In this app, we learn how to send a simple plain text message into the room that our app was  installed into.

## Getting set up

### Prerequisites ###

* Make sure you have [Node.js 8.x](https://nodejs.org/) or above on your machine (You can run `node --version` in your terminal to check what version you have)
* Make sure you have the latest version of [ngrok](https://ngrok.com/) installed (you can check if you have it installed by running `ngrok --version` in your terminal)
* Sign up for an account on [https://developer.atlassian.com](https://developer.atlassian.com).  You can also log in with your Atlassian Account if you already have on.
* Once logged in, click on your avatar in the upper right corner and click [Create app](https://developer.atlassian.com/apps/create). Give your app a name, there are no wrong names. You can ignore description and icon at this point.
* In the next step you'll be asked to Enable an Atlassian API for your app, click on **Enable API** for the Stride API.
* Once your app is created you need to:
  * Go into the 'App Features' tab and ensure that the **Enabled a bot account** checkbox is checked.
  * Go into the 'Enabled APIs' tab and copy the Stride API Client Id and Client Secret, we'll need this later.

### Running the app ###

* Clone this repository to your local machine. Run this command in your terminal `git clone git@bitbucket.org:atlassian/stride-message-example.git`
* Open a terminal window and `cd` into your local repository `stride-message-example` and run ```npm install```.
* While that's installing we'll need to set our Client Id and Client Secret as environment variables. In the root of the repository you cloned, copy .env_sample into a file called .env and paste in your Client Id and Client Secret into the appropriate spots.
* One npm finishes installing the node modules we can start up our app.  You can look in `package.json` for all the available scripts to run. We're going to start our app with logging turned on so we can see what's happening behind the scenes. Run `npm run verbose`.
* Now open a second terminal window and run ```ngrok http 3000```.  We need ngrok to expose our app running locally on our machine out to the internet so Stride can see it.
* Once ngrok starts up copy the https url from your ngrok output, this is where your app lives. You can get to the descriptor by navigating to the ngrok url + /descriptor (should look something like this `https://xxxxxxxx.ngrok.io/descriptor`)
* Go back to your app page on [https://developer.atlassian.com/apps](https://developer.atlassian.com/apps) and click on the app we created above
* Go into the 'Install' tab and paste your descriptor url in the *Descriptor Url* field.
* Click 'Refresh', you should see a green indicator
* Your app is now live and ready for use.

### Installing the app

* While you're still on the 'Install' tab, copy the **Installation URL**. This is the url we'll use to install the app in Stride.
* Go into a Stride room and click on the 'Apps' glance (the icon looks like a hexagon with a circle in the middle) on the right side of your screen, this will open up a sidebar.
* Click on the + icon at the top of the sidebar, this will open Strides app marketplace.
* Click on the 'Add custom app' link at the top of the page, this will open up a dialog where you can paste your installation URL. This will load your app's information into the dialog.
* Click 'Agree' to install the app.

### Seeing it in action

Once you install your app you should see a new message show up in the room.

``` text
stride-sample-app April 9, 2018 8:03 PM
🎉 The Stride sample app demonstrating how to send a message 💌 has been installed. 🚀
```

If you ran the app with `npm run verbose` you can go to the terminal and see all of the data that got passed to our app that was requested.  Installed payload and Authorization token.

## The code

There are a lot of files in the repository so lets get you to the right place in the code to look at what's going on.

### `routes/index.js`

In the route for `/installed` we make a call to `sendMessage` and pass it the message to print to the conversation in Stride. This function has been required in from the `/lib/sendMessage.js` module.

### `lib/sendMessage.js`

This module handles sending a message to the conversation. It has one function `sendMessage` and expects 4 arguments, but we only need to send in the first 2 to send an actual message. The other arguments allow us to inject mock dependencies so that we can unit test our function.

The first thing we need to do is get an access token.  We make a call to `accessToken` and that function is found in another module `/lib/accessToken.js`. We'll come back to that.

Once we have a token we can then make the REST call to Stride to send the message. Notice we're posting to an api.atlassian.com url that needs the `cloudId` and the `conversationId` that is being passed in with the context object we received from the installed payload.

### `/lib/accessToken.js`

In this module all we are doing is declaring a function that makes a call to receive a token.  We need our `Client_Id` and `Client_Secret` that we saved to the `.env` file to make an authenticated request for a token.

We post this to a `api.atlassian.com` url and we'll get back an object that contains the access token, the time till it expires, and other meta data. The token is passes back.

We can use this module in other calls other than `sendMessage`.

### Tests

There are a couple of provided tests in this repository.  We're using [jest](https://facebook.github.io/jest/) to run our tests. You can find the tests in the __tests__ folders.

We have tests for our descriptor and the routes we call.  We also have tests for the lib modules. To test these functions that make REST calls to Stride we need to create a mock function and pass that into our function to use that for making the requests. This allows us to mock the calls and test the functionality of the function separately.

You can run the tests by running the command `npm run test` in your terminal.

## Need help

Need help with this sample code or want to ask a question about Stride app development?  Head over to the [Atlassian Developer Community](https://community.developer.atlassian.com/) and create a new topic in the [Stride Development Category](https://community.developer.atlassian.com/c/stride-development).