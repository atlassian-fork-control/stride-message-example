const accessToken = require('../accessToken')

describe('Access token', () => {
  it('Access token is returned', () => {
    const mockProcess = {
      env: {
        CLIENT_ID: 'fake123',
        CLIENT_SECRET: 'fake123secret'
      }
    }
    const mockAxios = jest.fn().mockReturnValue(
      Promise.resolve({
        data: {
          access_token: 'xxx',
          expires_in: 3600,
          scope: 'participate:conversation',
          token_type: 'Bearer'
        }
      })
    )

    return accessToken(mockAxios, mockProcess).then(res => {
      expect(res.data.access_token).toBe('xxx')
      expect(mockAxios).toBeCalledWith({
        method: 'post',
        url: 'https://api.atlassian.com/oauth/token',
        data: {
          grant_type: 'client_credentials',
          client_id: mockProcess.env.CLIENT_ID,
          client_secret: mockProcess.env.CLIENT_SECRET
        }
      })
    })
  })
})
